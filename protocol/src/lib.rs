use anyhow::Context;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead, Write};

#[derive(Debug, Deserialize, Serialize)]
pub struct Message {
    pub src: String,
    pub dest: String,
    pub body: Body,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Body {
    #[serde(rename = "type")]
    pub kind: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub msg_id: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub in_reply_to: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(flatten)]
    pub rest: Option<HashMap<String, serde_json::Value>>,
}

impl Message {
    pub fn new(src: String, dest: String, body: Body) -> Self {
        Self { src, dest, body }
    }

    pub fn send_to(&self, out: &std::io::Stdout) -> anyhow::Result<()> {
        serde_json::to_writer(out.lock(), &self)?;
        out.lock().write_all(b"\n")?;
        Ok(())
    }
}

impl TryFrom<String> for Message {
    type Error = serde_json::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(serde_json::from_str(&value)?)
    }
}

#[derive(Debug)]
pub struct Node {
    pub id: Option<String>,
    pub messages: HashSet<u64>,
    pub message_counter: u64,
    pub neighbours: Option<Vec<String>>,
    pub gossiped: Option<HashMap<String, HashSet<u64>>>,
}

impl Node {
    pub async fn handle_incoming_messages(
        &mut self,
        try_respond: fn(&mut Self, Message) -> anyhow::Result<()>,
    ) -> anyhow::Result<()> {
        let stdin = io::stdin();
        for line in stdin.lock().lines() {
            let message = line?.try_into()?;
            try_respond(self, message)?;
        }
        Ok(())
    }

    pub fn handle_sending_message(&mut self, message: Message) -> anyhow::Result<()> {
        let stdout = io::stdout();
        self.message_counter += 1;
        message.send_to(&stdout)?;
        Ok(())
    }

    pub async fn gossip(&mut self) -> anyhow::Result<()> {
        let Some(neighbours) = &self.neighbours.clone() else {
            return Ok(())
        };

        for nei in neighbours {
            match &self.gossiped {
                Some(g) => match (*g).get(nei) {
                    Some(m) => {
                        let data_to_send: HashSet<u64> =
                            self.messages.difference(m).map(|x| *x).collect();
                        let dest_node = nei.clone();
                        let message_to_send =
                            self.generate_gossip_message(dest_node, data_to_send)?;
                        self.handle_sending_message(message_to_send)?;
                        return Ok(());
                    }
                    None => return Ok(()),
                },
                None => return Ok(()),
            }
        }
        Ok(())
    }

    pub fn generate_gossip_message(
        &mut self,
        dest_node: String,
        messages: HashSet<u64>,
    ) -> anyhow::Result<Message> {
        let src = self.id.clone().context("Node's id not present'")?;
        let mut messages: Vec<serde_json::Value> = messages
            .into_iter()
            .map(|m| serde_json::to_value(m).unwrap())
            .collect();
        let m = Message {
            src,
            dest: dest_node,
            body: Body {
                kind: "gossip".to_owned(),
                msg_id: Some(self.message_counter),
                in_reply_to: None,
                rest: Some(HashMap::from([(
                    "messages".to_owned(),
                    serde_json::to_value(&mut messages)?,
                )])),
            },
        };
        self.message_counter += 1;
        return Ok(m);
    }
}

impl Default for Node {
    fn default() -> Self {
        Self {
            id: None,
            messages: HashSet::new(),
            message_counter: 0,
            neighbours: None,
            gossiped: None,
        }
    }
}
