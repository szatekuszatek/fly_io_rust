use protocol::{Body, Message, Node};
use std::collections::HashMap;
use std::sync::Arc;
use tokio::time::{sleep, Duration};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let broadcast_node = Arc::new(tokio::sync::Mutex::new(Node::default()));
    let node_ref_1 = broadcast_node.clone();
    tokio::spawn(async move {
        let res = node_ref_1
            .lock()
            .await
            .handle_incoming_messages(try_respond)
            .await;
        if res.is_err() {
            println!("{:#?}", res);
        }
    });
    let node_ref_2 = broadcast_node.clone();
    tokio::spawn(async move {
        sleep(Duration::from_millis(50)).await;
        let res = node_ref_2.lock().await.gossip().await;
        if res.is_err() {
            println!("{:#?}", res);
        }
    });
    Ok(())
}

fn try_respond(
    node: &mut Node,
    Message {
        src,
        dest,
        body: Body {
            kind, msg_id, rest, ..
        },
        ..
    }: Message,
) -> anyhow::Result<()> {
    match kind.as_str() {
        "init" => {
            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "init_ok".to_string(),
                in_reply_to: msg_id,
                rest: None,
            };
            node.id = Some(dest.clone());
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        "broadcast" => {
            let payload = rest.expect("Invalid payload");
            let message = payload
                .get("message")
                .expect("Message was not provided")
                .as_u64()
                .expect("Message is not a number");
            node.messages.insert(message);
            node.gossiped.as_mut().map(|stored| {
                let stored_for_nei = stored.get_mut(&src);
                stored_for_nei.map(|s| s.insert(message))
            });

            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "broadcast_ok".to_string(),
                in_reply_to: msg_id,
                rest: None,
            };
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        "read" => {
            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "read_ok".to_string(),
                in_reply_to: msg_id,
                rest: Some(HashMap::from([(
                    "messages".to_string(),
                    serde_json::Value::from(node.messages.clone().into_iter().collect::<Vec<_>>()),
                )])),
            };
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        "topology" => {
            let payload = rest.expect("Invalid payload");
            let topology: HashMap<String, Vec<String>> = serde_json::from_value(
                payload
                    .get("topology")
                    .expect("Message was not provided")
                    .clone(),
            )?;

            node.neighbours = topology.get(&dest).cloned();

            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "topology_ok".to_string(),
                in_reply_to: msg_id,
                rest: None,
            };
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        _ => return Ok(()),
    };
    Ok(())
}
