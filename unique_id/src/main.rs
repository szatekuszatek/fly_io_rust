use protocol::{Body, Message, Node};
use serde_json::Number;
use std::collections::HashMap;
use uuid::Uuid;

fn main() -> anyhow::Result<()> {
    let mut unique_id_node = Node::default();
    unique_id_node.handle_incoming_messages(try_respond)?;
    Ok(())
}

fn try_respond(
    node: &mut Node,
    Message {
        src,
        dest,
        body: Body { kind, msg_id, .. },
        ..
    }: Message,
) -> anyhow::Result<()> {
    match kind.as_str() {
        "init" => {
            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "init_ok".to_string(),
                in_reply_to: msg_id,
                rest: None,
            };
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        "generate" => {
            let new_id: Number = Uuid::new_v4().as_u64_pair().0.into();
            let rest =
                HashMap::from([("id".to_string(), serde_json::value::Value::Number(new_id))]);
            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "generate_ok".to_string(),
                in_reply_to: msg_id,
                rest: Some(rest),
            };
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        _ => return Ok(()),
    };
    Ok(())
}
