use protocol::{Body, Message, Node};
use std::io;

fn main() -> anyhow::Result<()> {
    let mut echo_node = Node::default();
    echo_node.handle_incoming_messages(try_respond)?;
    Ok(())
}

fn try_respond(
    node: &mut Node,
    Message {
        src,
        dest,
        body: Body {
            kind, msg_id, rest, ..
        },
        ..
    }: Message,
) -> anyhow::Result<()> {
    match kind.as_str() {
        "init" => {
            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "init_ok".to_string(),
                in_reply_to: msg_id,
                rest: None,
            };
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        "echo" => {
            let reply_body = Body {
                msg_id: Some(node.message_counter),
                kind: "echo_ok".to_string(),
                in_reply_to: msg_id,
                rest,
            };
            let reply_message = Message::new(dest, src, reply_body);
            node.handle_sending_message(reply_message)?;
        }
        _ => return Ok(()),
    };
    Ok(())
}
